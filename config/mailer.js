const mailgun = require('nodemailer-mailgun-transport')
const config = require('config')

console.log(config.mailgun)

const transport = mailgun({
  auth: {
    api_key: config.mailgun.apiKey,
    domain: config.mailgun.domain,
  },
})

console.log(transport)

module.exports = {
  transport,
}
