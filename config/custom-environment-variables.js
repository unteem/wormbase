module.exports = {
  'pubsweet-server': {
    host: 'PUBSWEET_HOST',
    port: 'PUBSWEET_PORT',
    secret: 'PUBSWEET_SECRET',
    db: {
      user: 'POSTGRES_USER',
      password: 'POSTGRES_PASSWORD',
      host: 'POSTGRES_HOST',
      database: 'POSTGRES_DB',
      port: 'POSTGRES_PORT',
    },
  },
  'mailgun': {
    apiKey: "MAILER_MAILGUN_KEY",
    domain: "MAILER_MAILGUN_DOMAIN",
  },
  'tazendra': {
    apiURL: "TAZENDRA_API_URL",
  },
}
