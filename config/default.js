const { deferConfig } = require('config/defer')
const path = require('path')
const winston = require('winston')

const components = require('./components.json')

const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      colorize: true,
      // level: 'debug',
    }),
  ],
})

module.exports = {
  authsome: {
    mode: path.join(__dirname, 'authsome.js'),
    teams: {
      author: {
        name: 'Author',
      },
      editor: {
        name: 'Editor',
      },
      editors: {
        name: 'Editors Global',
      },
    },
  },
  detectionMethodCorrelations: {
    antibody: ['antibodyUsed'],
    existingTransgene: ['transgeneUsed'],
    genomeEditing: ['variation'],
    inSituHybridization: ['inSituDetails'],
    newTransgene: [
      'genotype',
      'constructionDetails',
      'dnaSequence',
      'utr',
      'reporter',
      'backboneVector',
      'fusionType',
      'constructComments',
      'transgeneName',
      'strain',
      'coinjected',
      'injectionConcentration',
      'integratedBy',
    ],
  },
  mailer: {
    from: 'noreply@micropulication.org',
    path: `${__dirname}/mailer`,
  },
  publicKeys: [
    'pubsweet-client',
    'authsome',
    'pubsweet',
    'validations',
    'detectionMethodCorrelations',
  ],
  pubsweet: { components },
  'pubsweet-client': {
    API_ENDPOINT: '/api',
    isPublic: true,
  },
  'pubsweet-server': {
    baseUrl: deferConfig(
      cfg => `${cfg['pubsweet-server'].host}:${cfg['pubsweet-server'].port}`,
    ),
    enableExperimentalGraphql: true,
    logger,
    tokenExpiresIn: '360 days',
    uploads: 'uploads',
  },
  'tazendra': {
    apiURL: 'http://tazendra.caltech.edu/~azurebrd/cgi-bin/forms/datatype_objects.cgi',
  },
  schema: {},
  validations: path.join(__dirname, 'validations'),
}
