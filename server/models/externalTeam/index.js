const model = require('./externalTeam')

module.exports = {
  model,
  modelName: 'ExternalTeam',
}
