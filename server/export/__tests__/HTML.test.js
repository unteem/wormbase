const beautify = require('js-beautify').html
const omit = require('lodash/omit')

const HTMLExport = require('../HTML')
const manuscript = require('../../../test/helpers/manuscript')
const manuscriptHTML = require('../../../test/helpers/manuscriptHTMLOutput')

const imageSrc = manuscript.image.url.substring(1)

// Standardize format of HTML strings, so that they can be compared
const format = html =>
  beautify(html, {
    indent_size: 4,
    preserve_newlines: false,
  })

describe('HTML file creation from manuscript', () => {
  test('Should handle missing image src', () => {
    const output = HTMLExport(manuscript, null)
    expect(output).toBeNull()
  })

  test('Should handle missing manuscript', () => {
    const output = HTMLExport(null, imageSrc)
    expect(output).toBeNull()
  })

  test('Should handle empty manuscript', () => {
    const output = HTMLExport({}, imageSrc)
    expect(output).toBeNull()
  })

  test('Should handle missing title', () => {
    const input = omit(manuscript, 'title')
    let output = HTMLExport(input, imageSrc)
    expect(output).toBeNull()

    input.title = ''
    output = HTMLExport(input, imageSrc)
    expect(output).toBeNull()
  })

  test('Should handle missing or malformed authors', () => {
    const input = omit(manuscript, 'authors')
    let output = HTMLExport(input, imageSrc)
    expect(output).toBeNull()

    input.authors = []
    output = HTMLExport(input, imageSrc)
    expect(output).toBeNull()

    input.authors = [{ key: 'value' }]
    output = HTMLExport(input, imageSrc)
    expect(output).toBeNull()

    input.authors = [
      {
        affiliations: 'University of Chicago',
        credit: ['Software'],
      },
    ]
    output = HTMLExport(input, imageSrc)
    expect(output).toBeNull()
  })

  test('Should handle missing image caption', () => {
    const input = omit(manuscript, 'imageCaption')
    let output = HTMLExport(input, imageSrc)
    expect(output).toBeNull()

    input.imageCaption = ''
    output = HTMLExport(input, imageSrc)
    expect(output).toBeNull()
  })

  test('Should handle missing pattern description', () => {
    const input = omit(manuscript, 'patternDescription')
    let output = HTMLExport(input, imageSrc)
    expect(output).toBeNull()

    input.patternDescription = ''
    output = HTMLExport(input, imageSrc)
    expect(output).toBeNull()
  })

  test('Should handle missing methods', () => {
    const input = omit(manuscript, 'methods')
    let output = HTMLExport(input, imageSrc)
    expect(output).toBeNull()

    input.methods = ''
    output = HTMLExport(input, imageSrc)
    expect(output).toBeNull()
  })

  test('Should handle missing references', () => {
    const input = omit(manuscript, 'references')
    let output = HTMLExport(input, imageSrc)
    expect(output).toBeNull()

    input.references = ''
    output = HTMLExport(input, imageSrc)
    expect(output).toBeNull()
  })

  test('Should handle missing funding', () => {
    const input = omit(manuscript, 'funding')
    let output = HTMLExport(input, imageSrc)
    expect(output).toBeNull()

    input.funding = ''
    output = HTMLExport(input, imageSrc)
    expect(output).toBeNull()
  })

  test('Should return HTML string', () => {
    const output = HTMLExport(manuscript, imageSrc)
    expect(output).not.toBeNull()

    const result = format(output)
    const expected = format(manuscriptHTML)
    expect(result).toEqual(expected)
  })
})
