// import HTMLExport from '../services/HTMLExport'

const fs = require('fs')
const path = require('path')
const { promisify } = require('util')
const JSZip = require('jszip')

const logger = require('@pubsweet/logger')

const Manuscript = require('../manuscript/src/manuscript')
const HTMLExport = require('./HTML')

const readFile = promisify(fs.readFile)
const writeFile = promisify(fs.writeFile)

const exportEndpoints = app => {
  app.get('/api/export/:manuscriptId/html', async (req, res) => {
    const { manuscriptId } = req.params

    // Get manuscript
    let manuscript
    try {
      manuscript = await Manuscript.query().findById(manuscriptId)
    } catch (e) {
      logger.error(e)
      throw new Error(e)
    }

    // Define names and paths for files
    const htmlFileName = `${manuscriptId}.html`
    const htmlFilePath = path.join(__dirname, '_tempFileStorage', htmlFileName)

    const imageFileName = manuscript.image.url.substring(1)
    const imageFilePath = path.join(
      __dirname,
      '..',
      '..',
      'uploads',
      imageFileName,
    )

    const zip = new JSZip()
    const zipFileName = `${manuscript.id}.zip`
    const zipFilePath = path.join(__dirname, '_tempFileStorage', zipFileName)

    const deleteFiles = () => {
      const filesToDelete = [htmlFilePath, zipFilePath]

      filesToDelete.forEach(file => {
        if (fs.existsSync(file)) {
          fs.unlink(file, err => {
            if (err) {
              logger.error(err)
              return
            }

            logger.info(`${file} deleted from temp folder`)
          })
        }
      })
    }

    const createZipFile = callback => {
      zip
        .generateNodeStream({
          streamFiles: true,
          type: 'nodebuffer',
        })
        .pipe(fs.createWriteStream(zipFilePath))
        .on('finish', () => {
          logger.info(`Zip file created for manuscript ${manuscript.id}`)
          callback()
        })
    }

    // Make HTML file
    const html = HTMLExport(manuscript, imageFileName)
    await writeFile(htmlFilePath, html)
    logger.info(`HTML file created for manuscript ${manuscript.id}`)

    // Add HTML and image to zip
    await Promise.all([
      readFile(htmlFilePath).then(data => zip.file('index.html', data)),
      readFile(imageFilePath).then(data => zip.file(imageFileName, data)),
    ])
      .then(() => {
        // Send zip file back to client for download
        createZipFile(() => {
          res.download(zipFilePath, `microPublication_${manuscript.id}.zip`)
        })
      })
      .catch(e => {
        logger.error(e)
        deleteFiles()
      })

    // Delete HTML and zip files when done
    res.on('finish', deleteFiles)
  })
}

module.exports = exportEndpoints
