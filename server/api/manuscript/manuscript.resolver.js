const clone = require('lodash/clone')

const { Team } = require('pubsweet-server')

const Manuscript = require('../../manuscript/src/manuscript')
const ExternalTeam = require('../../models/externalTeam/externalTeam')
const notify = require('../../services/notify')

const newStatus = {
  decision: {
    accepted: false,
    rejected: false,
    revise: false,
  },
  scienceOfficer: {
    approved: null,
    pending: false,
  },
  submission: {
    datatypeSelected: false,
    full: false,
    initial: false,
  },
}

const createManuscript = async (_, variables, ctx) => {
  const { user: userId } = ctx

  const manuscript = await new Manuscript({
    status: newStatus,
  }).save()

  // TO DO -- change objectType to manuscript
  // Create teams for manuscript
  const teams = [
    // Author team with current user as member
    {
      members: [userId],
      name: `author-${manuscript.id}`,
      object: {
        objectId: manuscript.id,
        objectType: 'article',
      },
      teamType: 'author',
    },

    // Editor team
    {
      members: [],
      name: `editor-${manuscript.id}`,
      object: {
        objectId: manuscript.id,
        objectType: 'article',
      },
      teamType: 'editor',
    },

    // Science officer team
    {
      members: [],
      name: `science-officer-${manuscript.id}`,
      object: {
        objectId: manuscript.id,
        objectType: 'article',
      },
      teamType: 'scienceOfficer',
    },

    // Reviewer team
    {
      members: [],
      name: `reviewers-${manuscript.id}`,
      object: {
        objectId: manuscript.id,
        objectType: 'article',
      },
      teamType: 'reviewers',
    },

    // Invited reviewers team
    {
      members: [],
      name: `reviewers-invited-${manuscript.id}`,
      object: {
        objectId: manuscript.id,
        objectType: 'article',
      },
      teamType: 'reviewersInvited',
    },

    // Reviewers that accepted
    {
      members: [],
      name: `reviewers-accepted-${manuscript.id}`,
      object: {
        objectId: manuscript.id,
        objectType: 'article',
      },
      teamType: 'reviewersAccepted',
    },

    // Reviewers that rejected
    {
      members: [],
      name: `reviewers-rejected-${manuscript.id}`,
      object: {
        objectId: manuscript.id,
        objectType: 'article',
      },
      teamType: 'reviewersRejected',
    },
  ]

  /* 
    We unfortunately need to execute these serially, otherwise `user.teams`
    gets corrupted. Replace with `Promise.all` once you stop using `user.teams`.
  */
  /* eslint-disable-next-line no-restricted-syntax */
  for (const teamData of teams) {
    /* eslint-disable-next-line no-await-in-loop */
    await new Team(teamData).save()
  }

  const externalTeams = [
    // Reviewers that are not users that have been added to the reviewer list
    {
      manuscriptId: manuscript.id,
      members: [],
      teamType: 'externalReviewers',
    },

    // Reviewers that are not users that have been invited to join the system
    {
      manuscriptId: manuscript.id,
      members: [],
      teamType: 'externalReviewersInvited',
    },
  ]

  await Promise.all(externalTeams.map(t => ExternalTeam.query().insert(t)))

  return manuscript.id
}

/* NOTE: No auth on this resolver */
const sendChat = async (_, variables, context) => {
  const { input } = variables
  const { message, manuscriptId } = input
  const { user: userId } = context

  const newMessage = {
    content: message,
    timestamp: new Date(),
    user: userId,
  }

  const manuscript = await Manuscript.query().findById(manuscriptId)
  const { communicationHistory } = manuscript

  const chat = communicationHistory
    ? clone(manuscript.communicationHistory)
    : []

  chat.push(newMessage)

  await manuscript.$query().patch({ communicationHistory: chat })

  notify('chat', {
    manuscriptId,
    message: newMessage,
    userId,
  })

  return manuscript.id
}

module.exports = {
  createManuscript,
  sendChat,
}
