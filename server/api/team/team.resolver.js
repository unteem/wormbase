/* eslint-disable import/prefer-default-export */
const without = require('lodash/without')

const { Team } = require('pubsweet-server/src/models')

/* 
  This ensures that if there are any assigned editors or science officers
  assigned to a manuscript that have been removed from the global teams,
  they are unassigned.
*/
const cleanUpGlobalTeamMembership = async (_, __, ctx) => {
  const globalTeams = await Team.findByField({ global: true })
  const globalEditorTeam = globalTeams.find(t => t.teamType === 'editors')
  const globalScienceOfficerTeam = globalTeams.find(
    t => t.teamType === 'scienceOfficers',
  )

  const teams = await Team.all()
  const editorTeams = teams.filter(t => t.teamType === 'editor')
  const scienceOfficerTeams = teams.filter(t => t.teamType === 'scienceOfficer')

  editorTeams.forEach(t => {
    t.members.forEach(m => {
      if (!globalEditorTeam.members.includes(m)) {
        ctx.connectors.Team.update(
          t.id,
          { members: without(t.members, m) },
          ctx,
        )
      }
    })
  })

  scienceOfficerTeams.forEach(t => {
    t.members.forEach(m => {
      if (!globalScienceOfficerTeam.members.includes(m)) {
        ctx.connectors.Team.update(
          t.id,
          { members: without(t.members, m) },
          ctx,
        )
      }
    })
  })

  return true
}

module.exports = { cleanUpGlobalTeamMembership }
