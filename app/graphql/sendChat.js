/* eslint-disable react/prop-types */

import React from 'react'
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'

import { GET_ARTICLE_FOR_EDITOR } from '../components/compose/pieces/getArticleForEditor'

const SEND_CHAT = gql`
  mutation SendChat($input: SendChatInput!) {
    sendChat(input: $input)
  }
`

const SendChatMutation = props => {
  const { articleId, render } = props

  const refetchQueries = [
    {
      query: GET_ARTICLE_FOR_EDITOR,
      variables: { id: articleId },
    },
  ]

  return (
    <Mutation mutation={SEND_CHAT} refetchQueries={refetchQueries}>
      {(sendChat, sendChatResponse) => render({ sendChat, sendChatResponse })}
    </Mutation>
  )
}

export default SendChatMutation
