/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

const Wrapper = styled.div`
  display: flex;
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  margin-bottom: ${th('gridUnit')};
`

const Side = styled.div`
  display: flex;
  flex-direction: column;

  span {
    padding: 2px 0;
  }
`

const Role = styled.span`
  align-self: right;
  flex-basis: 25%;
  font-variant-ligatures: none;
  margin-right: ${th('gridUnit')};
  text-align: right;

  &::after {
    content: ':';
  }
`

const Name = styled.span`
  color: ${th('colorPrimary')};
`

const NotAssigned = styled.span`
  color: ${th('colorError')};
  text-transform: uppercase;
`

const getName = user => (user ? user.username : null)
const NotAssignedMessage = () => <NotAssigned>not assigned</NotAssigned>

const PanelInfo = props => {
  const { author, editor, scienceOfficer } = props

  const items = [
    {
      label: 'Editor',
      value: getName(editor),
    },
    {
      label: 'Science Officer',
      value: getName(scienceOfficer),
    },
    {
      label: 'Author',
      value: `${author.name} ( ${author.email} )`,
    },
  ]

  return (
    <Wrapper>
      <Side>
        {items.map(item => (
          <Role>{item.label}</Role>
        ))}
      </Side>

      <Side>
        {items.map(
          item =>
            item.value ? <Name>{item.value}</Name> : <NotAssignedMessage />,
        )}
      </Side>
    </Wrapper>
  )
}

export default PanelInfo
