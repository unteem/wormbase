/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'

import { DateParser } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

const Wrapper = styled.span`
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeighBaseSmall')};
`

const Label = styled.span`
  color: ${th('colorBorder')};
  margin-right: calc(${th('gridUnit')} / 2);
  text-transform: capitalize;

  &:before {
    content: '\\2014';
    padding: 0 ${th('gridUnit')};
  }

  &:after {
    content: ':';
  }
`

const Date = props => {
  const { label, value } = props

  return (
    <Wrapper>
      <Label>{label}</Label>
      <DateParser humanizeThreshold={3} timestamp={value}>
        {timestamp => <span>{timestamp}</span>}
      </DateParser>
    </Wrapper>
  )
}

export default Date
