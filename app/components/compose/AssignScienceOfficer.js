/* eslint-disable react/prop-types */

import React from 'react'
import { adopt } from 'react-adopt'

import {
  getGlobalTeams,
  getTeamsForArticle,
  updateCurrentlyWith,
  updateTeam,
} from './pieces'

const mapper = {
  getGlobalTeams,
  getTeams: props => getTeamsForArticle(props),
  updateCurrentlyWith,
  updateTeam,
}

const getAllScienceOfficers = globalTeams =>
  globalTeams && globalTeams.find(t => t.teamType === 'scienceOfficers').members

const getScienceOfficerTeamForArticle = teamsForArticle =>
  teamsForArticle && teamsForArticle.find(t => t.teamType === 'scienceOfficer')

const getScienceOfficer = team => {
  if (!team) return null
  if (team.members.length > 0) return team.members[0]
  return undefined
}

const mapProps = args => {
  const scienceOfficerTeamForArticle = getScienceOfficerTeamForArticle(
    args.getTeams.data.teamsForArticle,
  )

  const allScienceOfficerIds = getAllScienceOfficers(
    args.getGlobalTeams.data.globalTeams,
  ).map(so => so.id)

  const scienceOfficer = getScienceOfficer(scienceOfficerTeamForArticle)
  // console.log('science officer', scienceOfficerTeamForArticle)

  return {
    allScienceOfficers: getAllScienceOfficers(
      args.getGlobalTeams.data.globalTeams,
    ),
    loading: args.getGlobalTeams.loading || args.getTeams.loading,
    scienceOfficer:
      scienceOfficer && allScienceOfficerIds.includes(scienceOfficer.id)
        ? scienceOfficer
        : null,
    scienceOfficerTeamId:
      scienceOfficerTeamForArticle && scienceOfficerTeamForArticle.id,
    updateCurrentlyWith: args.updateCurrentlyWith.updateCurrentlyWith,
    updateTeam: args.updateTeam.updateTeam,
  }
}

const Composed = adopt(mapper, mapProps)

const ComposedAssignScienceOfficer = props => {
  const { render, ...otherProps } = props

  return (
    <Composed {...otherProps}>
      {mappedProps => render({ ...mappedProps, ...otherProps })}
    </Composed>
  )
}

export default ComposedAssignScienceOfficer
