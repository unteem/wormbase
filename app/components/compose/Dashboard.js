/* eslint-disable react/prop-types */

import React from 'react'
import { Adopt } from 'react-adopt'
import { withApollo } from 'react-apollo'
import { withRouter } from 'react-router-dom'
import { get } from 'lodash'

import { withCurrentUser } from '../../userContext'

import {
  createManuscript,
  createReview,
  deleteArticle,
  getDashboardArticles as getDashboardArticlesQuery,
  getGlobalTeams as getGlobalTeamsQuery,
  handleInvitation,
} from './pieces'

/* eslint-disable sort-keys */
const mapper = {
  createManuscript,
  createReview,
  deleteArticle,
  getDashboardArticlesQuery,
  getGlobalTeamsQuery,
  handleInvitation,
}
/* eslint-enable sort-keys */

const getTeamByType = (teams, type) =>
  teams && teams.find(t => t.teamType === type)

/* eslint-disable-next-line arrow-body-style */
const mapProps = args => {
  return {
    authorArticles: get(
      args.getDashboardArticlesQuery,
      'data.dashboardArticles.author',
    ),
    createManuscript: args.createManuscript.createManuscript,
    createReview: args.createReview.createReview,
    deleteArticle: args.deleteArticle.deleteArticle,
    editorArticles: get(
      args.getDashboardArticlesQuery,
      'data.dashboardArticles.editor',
    ),
    globalEditorTeam: getTeamByType(
      args.getGlobalTeamsQuery.data.globalTeams,
      'editors',
    ),
    handleInvitation: args.handleInvitation.handleInvitation,
    isGlobal: get(
      args.getDashboardArticlesQuery,
      'data.dashboardArticles.isGlobal',
    ),
    loading:
      args.getGlobalTeamsQuery.loading ||
      args.getDashboardArticlesQuery.loading,
    reviewerArticles: get(
      args.getDashboardArticlesQuery,
      'data.dashboardArticles.reviewer',
    ),
  }
}

const Composed = ({ currentUser, render, ...props }) => (
  <Adopt mapper={mapper} mapProps={mapProps}>
    {mappedProps => render({ ...props, ...mappedProps, currentUser })}
  </Adopt>
)

export default withApollo(withRouter(withCurrentUser(Composed)))
