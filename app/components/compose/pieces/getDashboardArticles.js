/* eslint-disable react/prop-types */

import React from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'

import { withCurrentUser } from '../../../userContext'

const GET_DASHBOARD_ARTICLES = gql`
  query GetDashboardArticles($currentUserId: ID!) {
    dashboardArticles(currentUserId: $currentUserId) {
      author {
        created
        id
        status {
          decision {
            accepted
            rejected
            revise
          }
          reviewers {
            accepted
            invited
            submitted
          }
          scienceOfficer {
            approved
            pending
          }
          submission {
            datatypeSelected
            initial
            full
          }
        }
        title
        updated
      }
      editor {
        assignedEditor {
          id
          username
        }
        created
        id
        status {
          decision {
            accepted
            rejected
            revise
          }
          reviewers {
            accepted
            invited
            submitted
          }
          scienceOfficer {
            approved
            pending
          }
          submission {
            datatypeSelected
            initial
            full
          }
        }
        title
        updated
      }
      isGlobal
      reviewer {
        acknowledgements
        authors {
          affiliations
          name
        }
        comments
        created
        datatype
        funding
        geneExpression {
          antibodyUsed
          backboneVector {
            name
          }
          coinjected
          constructComments
          constructionDetails
          detectionMethod
          dnaSequence {
            name
          }
          expressionPattern {
            name
          }
          fusionType {
            name
          }
          genotype
          injectionConcentration
          inSituDetails
          integratedBy {
            name
          }
          observeExpression {
            certainly {
              certainly {
                name
              }
              during {
                name
              }
              id
              subcellularLocalization {
                name
              }
            }
            partially {
              partially {
                name
              }
              during {
                name
              }
              id
              subcellularLocalization {
                name
              }
            }
            possibly {
              possibly {
                name
              }
              during {
                name
              }
              id
              subcellularLocalization {
                name
              }
            }
            not {
              not {
                name
              }
              during {
                name
              }
              id
              subcellularLocalization {
                name
              }
            }
          }
          reporter {
            name
          }
          species {
            name
          }
          strain
          transgeneName
          transgeneUsed {
            name
          }
          utr {
            name
          }
          variation {
            name
          }
        }
        id
        image {
          name
          url
        }
        imageCaption
        methods
        patternDescription
        references
        reviewerStatus
        title
        updated
      }
    }
  }
`

const GetDashboardArticlesQuery = props => {
  const { currentUser, render } = props

  const variables = {
    currentUserId: currentUser.id,
  }

  return (
    <Query query={GET_DASHBOARD_ARTICLES} variables={variables}>
      {render}
    </Query>
  )
}

export { GET_DASHBOARD_ARTICLES }
export default withCurrentUser(GetDashboardArticlesQuery)
