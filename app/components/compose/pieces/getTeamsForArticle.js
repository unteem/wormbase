/* eslint-disable react/prop-types */

import React from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'

const GET_TEAMS_FOR_ARTICLE = gql`
  query GetTeamsForArticle($id: ID!) {
    teamsForArticle(id: $id) {
      id
      members {
        id
        username
      }
      name
      teamType
    }
  }
`

const getTeamsForArticleQuery = props => {
  const { articleId: id, render } = props

  return (
    <Query query={GET_TEAMS_FOR_ARTICLE} variables={{ id }}>
      {render}
    </Query>
  )
}

export { GET_TEAMS_FOR_ARTICLE }
export default getTeamsForArticleQuery
