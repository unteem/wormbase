/* eslint-disable react/prop-types */

import React from 'react'
import { adopt } from 'react-adopt'
import { withRouter } from 'react-router-dom'
import { get, union } from 'lodash'

import {
  getArticleForEditor,
  getGlobalTeams,
  getExternalTeamsForManuscript,
  getReviewsForArticle,
  getTeamsForArticle,
  manuscriptMetadataUpdate,
  updateArticleForEditor,
} from './pieces'

import { sendChatMutation } from '../../graphql'

import { withCurrentUser } from '../../userContext'
import {
  getEditor,
  getReviewersTeamByType,
  getScienceOfficer,
} from '../../helpers/teams'

const mapper = {
  getArticleForEditor: props => getArticleForEditor(props),
  getExternalTeamsForManuscript: props => getExternalTeamsForManuscript(props),
  getGlobalTeams,
  getReviewsForArticle: props => getReviewsForArticle(props),
  getTeamsForArticle: props => getTeamsForArticle(props),
  manuscriptMetadataUpdate,
  sendChatMutation,
  updateArticleForEditor,
}

const getAllScienceOfficers = globalTeams =>
  globalTeams && globalTeams.find(t => t.teamType === 'scienceOfficers').members

const getAllEditors = globalTeams =>
  globalTeams && globalTeams.find(t => t.teamType === 'editors').members

const mapProps = args => {
  const teams = get(args.getTeamsForArticle, 'data.teamsForArticle')
  const externalTeams = get(
    args.getExternalTeamsForManuscript,
    'data.getExternalTeamsForManuscript',
  )

  const getTeamMembers = type =>
    get(getReviewersTeamByType(teams, type), 'members')

  const getExternalTeamMembers = type =>
    get(getReviewersTeamByType(externalTeams, type), 'members')

  const reviewersTeam = getTeamMembers('reviewers')
  const invitedReviewers = getTeamMembers('reviewersInvited')
  const acceptedReviewers = getTeamMembers('reviewersAccepted')
  const rejectedReviewers = getTeamMembers('reviewersRejected')

  const allScienceOfficerIds =
    args.getGlobalTeams.data.globalTeams &&
    getAllScienceOfficers(args.getGlobalTeams.data.globalTeams).map(so => so.id)

  const scienceOfficer = getScienceOfficer(
    args.getTeamsForArticle.data.teamsForArticle,
  )

  const allEditorIds =
    args.getGlobalTeams.data.globalTeams &&
    getAllEditors(args.getGlobalTeams.data.globalTeams).map(e => e.id)

  const editor = getEditor(args.getTeamsForArticle.data.teamsForArticle)

  const externalReviewersTeam = getExternalTeamMembers('externalReviewers')
  const externalReviewersInvited = getExternalTeamMembers(
    'externalReviewersInvited',
  )
  const externalReviewersAccepted = getExternalTeamMembers(
    'externalReviewersAccepted',
  )
  const externalReviewersRejected = getExternalTeamMembers(
    'externalReviewersRejected',
  )

  const invitedCount = union(invitedReviewers, externalReviewersInvited).length
  const acceptedCount = union(acceptedReviewers, externalReviewersAccepted)
    .length
  const rejectedCount = union(rejectedReviewers, externalReviewersRejected)
    .length

  return {
    article: args.getArticleForEditor.data.manuscript,
    editor: editor && allEditorIds.includes(editor.id) ? editor : null,
    editorSuggestedReviewers: union(reviewersTeam, externalReviewersTeam),
    loading:
      args.getTeamsForArticle.loading ||
      args.getArticleForEditor.loading ||
      args.getReviewsForArticle.loading,
    reviewerCounts: {
      accepted: acceptedCount,
      invited: invitedCount,
      rejected: rejectedCount,
    },
    reviews: get(args.getReviewsForArticle, 'data.reviewsForArticle'),
    scienceOfficer:
      scienceOfficer && allScienceOfficerIds.includes(scienceOfficer.id)
        ? scienceOfficer
        : null,
    sendChat: args.sendChatMutation.sendChat,
    updateArticle: args.updateArticleForEditor.updateArticle,
    updateMetadata:
      args.manuscriptMetadataUpdate.manuscriptMetadataUpdateMutation,
  }
}

const Composed = adopt(mapper, mapProps)

const ComposedEditorPanel = props => {
  const { currentUser, match, render, ...otherProps } = props
  const { id: articleId } = match.params

  return (
    <Composed articleId={articleId} {...otherProps}>
      {mappedProps => render({ articleId, currentUser, ...mappedProps })}
    </Composed>
  )
}

// TO DO -- delete withRouter and get article id from getArticle

export default withRouter(withCurrentUser(ComposedEditorPanel))
