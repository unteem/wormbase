/* eslint-disable react/prop-types */

import React from 'react'
import { get } from 'lodash'
import { Toggle } from 'react-powerplug'

import { Button } from '@pubsweet/ui'
import Authorize from 'pubsweet-client/src/helpers/AuthorizeWithGraphQL'

import {
  getCurrentStatus,
  isDatatypeSelected,
  isFullSubmissionReady,
  isInitialSubmissionReady,
  isUnderRevision,
} from '../helpers/status'

import { ArticlePreviewModal } from './ui'
import { SubmissionConfirmationModal } from '../ui'
import Dropdown from './formElements/Dropdown'
import InitialSubmission from './formElements/InitialSubmission'
import GeneExpressionForm from './formElements/GeneExpressionForm'

const options = {
  dataType: [
    {
      label: 'Gene expression results',
      value: 'geneExpression',
    },
    {
      label: 'No Datatype',
      value: 'noDatatype',
    },
  ],
}

const isReadOnly = status => {
  const currentStatus = getCurrentStatus(status)

  if (
    currentStatus === 'Initial submission ready' ||
    currentStatus === 'Submitted'
  )
    return true

  return false
}

const DatatypeSelect = props => {
  const { article, values } = props

  const disabledSelect = (
    <Dropdown
      error={get(props.errors, 'dataType')}
      isDisabled
      label="Choose a datatype"
      name="dataType"
      options={options.dataType}
      required
      touched={get(props.touched, 'dataType')}
      value={options.dataType.find(o => o.value === get(values, 'dataType'))}
      {...props}
    />
  )

  return (
    <Authorize
      object={article}
      operation="isGlobal"
      unauthorized={disabledSelect}
    >
      <Dropdown
        error={get(props.errors, 'dataType')}
        label="Choose a datatype"
        name="dataType"
        options={options.dataType}
        required
        touched={get(props.touched, 'dataType')}
        value={options.dataType.find(o => o.value === get(values, 'dataType'))}
        {...props}
      />
    </Authorize>
  )
}

const SubmissionForm = props => {
  const { article, /* dirty, */ handleSubmit, isValid, values } = props

  const { status } = values
  const readOnly = isReadOnly(status)
  const datatypeSelected = isDatatypeSelected(status)
  const initial = isInitialSubmissionReady(status)
  const full = isFullSubmissionReady(status)

  const initialSubmissionState = !initial
  const dataTypeSelectionState = initial && !datatypeSelected
  const fullSubmissionState = initial && datatypeSelected && !full
  const revisionState = isUnderRevision(status)

  return (
    <React.Fragment>
      <InitialSubmission readOnly={readOnly} values={values} {...props} />

      {dataTypeSelectionState && (
        <DatatypeSelect article={article} values={values} {...props} />
      )}

      {(fullSubmissionState || revisionState) &&
        values.dataType === 'geneExpression' && (
          <Authorize object={article} operation="isAuthor" unauthorized={null}>
            <GeneExpressionForm readOnly={readOnly} {...props} />
          </Authorize>
        )}

      <Toggle initial={false}>
        {({ on, toggle }) => (
          <React.Fragment>
            <Button onClick={toggle}>Preview</Button>
            <ArticlePreviewModal
              isOpen={on}
              onRequestClose={toggle}
              values={values}
            />
          </React.Fragment>
        )}
      </Toggle>

      {(initialSubmissionState || fullSubmissionState || revisionState) &&
        isValid && (
          <Toggle intial={false}>
            {({ on, toggle }) => (
              <React.Fragment>
                <Button
                  /* disabled={dirty && !isValid} */
                  onClick={toggle}
                  primary
                >
                  Submit
                </Button>

                <SubmissionConfirmationModal
                  initial={initialSubmissionState}
                  isOpen={on}
                  onConfirm={handleSubmit}
                  onRequestClose={toggle}
                />
              </React.Fragment>
            )}
          </Toggle>
        )}

      {(dataTypeSelectionState || !isValid) && (
        <Button primary type="submit">
          Submit
        </Button>
      )}
    </React.Fragment>
  )
}

export default SubmissionForm
