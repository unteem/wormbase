/* eslint-disable react/prop-types */

import React, { Fragment } from 'react'
import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

import ComposedAssignScienceOfficer from '../compose/AssignScienceOfficer'
import Select from '../ui/Select'

const Label = styled.div`
  align-self: center;
  margin: 0 ${th('gridUnit')};
`

const mapUserToSelect = user =>
  user && {
    label: user.username,
    value: user.id,
  }

const mapUsersToOptions = users =>
  users && users.map(user => mapUserToSelect(user))

const AssignEditor = props => {
  const {
    allScienceOfficers,
    articleId,
    scienceOfficer,
    scienceOfficerTeamId,
    loading,
    updateCurrentlyWith,
    updateTeam,
  } = props
  if (loading) return null

  const options = mapUsersToOptions(allScienceOfficers)
  const value = mapUserToSelect(scienceOfficer)

  const handleChange = newValue => {
    const input = { members: [newValue.value] }
    updateTeam({ variables: { id: scienceOfficerTeamId, input } }).then(() => {
      const editorId = newValue.value

      const data = {
        currentlyWith: editorId,
        id: articleId,
      }

      updateCurrentlyWith({ variables: { data } })
    })
  }

  return (
    <Fragment>
      <Label>Science Officer</Label>
      <Select
        isSearchable={false}
        onChange={handleChange}
        options={options}
        value={value}
      />
    </Fragment>
  )
}

const Composed = ({ articleId }) => (
  <ComposedAssignScienceOfficer articleId={articleId} render={AssignEditor} />
)

export default Composed
