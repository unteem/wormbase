/* eslint-disable react/prop-types */

import React from 'react'
import { get, cloneDeep } from 'lodash'

import Form from './Form'

import { dataToFormValues, formValuesToData } from '../formElements/helpers'
import makeSchema from '../formElements/validations'
import { updateSubmissionStatus } from '../../helpers/status'

const SubmissionForm = props => {
  const { article, showModal, update, upload, ...otherProps } = props
  const initialValues = dataToFormValues(article)
  const validations = makeSchema(initialValues)

  const handleSubmit = (formValues, formikBag) => {
    const submit = () => {
      const data = cloneDeep(formValues)
      data.status = updateSubmissionStatus(data.status, data.dataType)
      const manuscriptInput = formValuesToData(data)

      update({ variables: { data: manuscriptInput } })
      formikBag.resetForm(formValues)

      const { datatypeSelected, full } = data.status.submission

      if (
        !(datatypeSelected && !full) &&
        !(full && data.dataType === 'noDatatype')
      ) {
        showModal()
      }
    }

    if (get(formValues, 'image.url') === get(article, 'image.url')) {
      // eslint-disable-next-line no-param-reassign
      delete formValues.image
      submit()
    } else {
      upload({
        variables: { file: formValues.image.file },
      }).then(res => {
        // const imageName = formValues.image.file.name
        const imageName = formValues.image.name

        // eslint-disable-next-line no-param-reassign
        formValues.image = {
          name: imageName,
          url: res.data.upload.url,
        }

        submit()
      })
    }
  }

  const isInitialValid = validations.isValidSync(initialValues)

  return (
    <Form
      enableReinitialize
      initialValues={initialValues}
      isInitialValid={isInitialValid}
      onSubmit={handleSubmit}
      validationSchema={validations}
      {...otherProps}
    />
  )
}

export default SubmissionForm
