FROM node:8.14-alpine

WORKDIR /app

RUN apk add --no-cache --virtual .gyp python make g++ git

RUN yarn config set yarn-offline-mirror /npm-packages-offline-cache && \
    yarn config set prefer-offline true && \
    yarn config set yarn-offline-mirror-pruning true

COPY package.json yarn.lock ./ 

RUN yarn install --frozen-lockfile

COPY webpack webpack
COPY static static
COPY server server
COPY app app
COPY config config

ENV NODE_ENV production

# we need to define some dummy env or build will fail, those variable will be re-set at runtime 
ENV MAILER_MAILGUN_KEY production
ENV MAILER_MAILGUN_DOMAIN production

RUN yarn pubsweet build

COPY app.js app.js
COPY e2e e2e
COPY scripts scripts
COPY test test


CMD ["node", "app.js"]
